import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('sidenav') sidenav: MatSidenav;
  isMenuOpened = true;
  showSubmenu: boolean = false;
  isShowing = false;
  showSubSubMenu: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }
  
  mouseenter() {
    if (!this.isMenuOpened) {
      this.isShowing = true;
    }
  }

  mouseleave() {
    if (!this.isMenuOpened) {
      this.isShowing = false;
    }
  }
}
